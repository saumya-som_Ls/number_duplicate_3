let str = "onetwothreefourfivesixsevenoneeightnineteneno";
let total = {
  One: 0,
  Two: 0,
  Three: 0,
  Four: 0,
  Five: 0,
  Six: 0,
  Seven: 0,
  Eight: 0,
  Nine: 0,
  Ten: 0,
};

let count = (str.match(/w/g) || []).length;
total.Two = count;

for (i = 0; i < count; i++) {
  str = str.replace("t", "");
  str = str.replace("w", "");
  str = str.replace("o", "");
}

let count1 = (str.match(/u/g) || []).length;
total.Four = count1;

for (i = 0; i < count1; i++) {
  str = str.replace("f", "");
  str = str.replace("o", "");
  str = str.replace("u", "");
  str = str.replace("r", "");
}

let count2 = (str.match(/x/g) || []).length;
total.Six = count2;

for (i = 0; i < count2; i++) {
  str = str.replace("s", "");
  str = str.replace("i", "");
  str = str.replace("x", "");
}

let count3 = (str.match(/g/g) || []).length;
total.Eight = count3;

for (i = 0; i < count2; i++) {
  str = str.replace("e", "");
  str = str.replace("i", "");
  str = str.replace("g", "");
  str = str.replace("h", "");
  str = str.replace("t", "");
}

let count4 = (str.match(/f/g) || []).length;
total.Five = count4;

for (i = 0; i < count4; i++) {
  str = str.replace("f", "");
  str = str.replace("i", "");
  str = str.replace("v", "");
  str = str.replace("e", "");
}

let count5 = (str.match(/s/g) || []).length;
total.Seven = count5;

for (i = 0; i < count5; i++) {
  str = str.replace("s", "");
  str = str.replace("e", "");
  str = str.replace("v", "");
  str = str.replace("e", "");
  str = str.replace("n", "");
}

let count6 = (str.match(/o/g) || []).length;
total.One = count6;

for (i = 0; i < count6; i++) {
  str = str.replace("o", "");
  str = str.replace("n", "");
  str = str.replace("e", "");
}

let count7 = (str.match(/i/g) || []).length;
total.Nine = count7;

for (i = 0; i < count7; i++) {
  str = str.replace("n", "");
  str = str.replace("i", "");
  str = str.replace("n", "");
  str = str.replace("e", "");
}

let count8 = (str.match(/h/g) || []).length;
total.Three = count8;

for (i = 0; i < count8; i++) {
  str = str.replace("t", "");
  str = str.replace("h", "");
  str = str.replace("r", "");
  str = str.replace("e", "");
  str = str.replace("e", "");
}

let count9 = (str.match(/e/g) || []).length;
total.Ten = count9;

for (i = 0; i < count9; i++) {
  str = str.replace("t", "");
  str = str.replace("e", "");
  str = str.replace("n", "");
}

console.log(total);
console.log(str);
